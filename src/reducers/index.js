import {
    LOG_IN,
    GET_ALL_ID, GET_ALL_USER, CHANGE_SCREEN, ADD_POST, SET_LOADING
} from "../constants/action-types";

const initialState = {
    loading: false,
    user: {
        id: 1,
        username: ""
    },
    posts: [],
    screen: 'login'
};

export function rootReducer(state = initialState, action) {
    if (action.type === LOG_IN) {
        return Object.assign({}, state, {
            loading: false,
            user: action.payload.user,
            screen: action.payload.screen,
        });
    }
    if (action.type === GET_ALL_ID) {
        return Object.assign({}, state, {
            loading: false,
            posts: action.payload.posts,
        });
    }
    if (action.type === GET_ALL_USER) {
        return Object.assign({}, state, {
            loading: false,
            posts: action.payload.posts,
        });
    }
    if (action.type === CHANGE_SCREEN) {
        return Object.assign({}, state, {
            loading: false,
            screen: action.payload.screen,
        });
    }
    if (action.type === ADD_POST) {
        console.log(action.payload.screen);
        return Object.assign({}, state, {
            loading: false,
            screen: action.payload.screen,
        });
    }
    if (action.type === SET_LOADING) {
        return Object.assign({}, state, {
            loading: action.payload.loading,
        });
    }

    return state;
}

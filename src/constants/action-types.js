export const LOG_IN = "LOG_IN";
export const GET_ALL_ID = "GET_ALL_ID";
export const GET_ALL_USER = "GET_ALL_USER";
export const CHANGE_SCREEN = "CHANGE_SCREEN";
export const ADD_POST = 'ADD_POST';
export const SET_LOADING = 'SET_LOADING';
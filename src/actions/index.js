import {
    LOG_IN,
    GET_ALL_ID,
    GET_ALL_USER,
    CHANGE_SCREEN,
    ADD_POST, SET_LOADING
} from "../constants/action-types";
import axios from 'axios';

export function logIn(payload) {
    return (dispatch) => {
        axios.get(`http://192.168.43.38:8080/login/${payload.username}/${payload.password}`)
            .then(res => {
                const user = res.data;
                if(user !== null)
                {
                    dispatch({
                        type:LOG_IN,
                        payload: {
                            user: user,
                            screen: 'home'
                        }
                    })
                }
            })
            .catch(function (error) {
                console.log(error)
            });
    }
}

export function getAll(key, order) {
    setLoading(true);
    return (dispatch) => {
        axios.get(`http://192.168.43.38:8080/posts/get/${key}/${order}`)
            .then(res => {
                const posts = res.data;
                if(posts !== null)
                {
                    dispatch({
                        type:GET_ALL_ID,
                        payload: {
                            posts: posts
                        }
                    });
                    setLoading(false);
                }
            })
            .catch(function (error) {
                console.log(error)
            });
    }
}

export function getAllUser(id, key, order) {
    return (dispatch) => {
        axios.get(`http://192.168.43.38:8080/posts/user/${id}/${key}/${order}`)
            .then(res => {
                const posts = res.data;
                if(posts !== null)
                {
                    dispatch({
                        type:GET_ALL_USER,
                        payload: {
                            posts: posts
                        }
                    })
                }
            })
            .catch(function (error) {
                console.log(error)
            });
    }
}

export function deletePost(userId, postId) {
    console.log(userId, "---", postId);
    return (dispatch) => {
        axios.get(`http://192.168.43.38:8080/posts/delete/${userId}/${postId}`)
            .then(res => {
                const posts = res.data;
                if(posts !== null)
                {
                    dispatch({
                        type:GET_ALL_USER,
                        payload: {
                            posts: posts
                        }
                    })
                }
            })
            .catch(function (error) {
                console.log(error)
            });
    }
}

export function addPost(post) {
    return (dispatch) => {
        axios.post(`http://192.168.43.38:8080/posts/post`, post)
            .then(res => {
                const response = res.data;
                console.log(response);
                console.log('responseeeee');
                if(response.status === 'success')
                {
                    dispatch({
                        type:CHANGE_SCREEN,
                        payload: {
                            screen: 'user'
                        }
                    });
                }
            })
            .catch(function (error) {
                console.log(error)
            });
    }
}

export function changeScreen(screen) {
    return {
            type:CHANGE_SCREEN,
            payload: {
                screen: screen
            }
    }
}

export function setLoading(loading) {
    return {
        type:SET_LOADING,
        payload: {
            loading: loading
        }
    }
}


import React, {Component} from 'react';
import { connect } from 'react-redux';
import {Container, Header, Content, Body, Title, Subtitle, Text, Button, View, Footer} from 'native-base'
import ExtraPostCard from '../other/extraPostCard'
import {changeScreen, getAllUser} from "../../actions";



class AppUserScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            order: 'ASC'
        }
    }

    componentDidMount(): void {
        this.getPosts('id');

    }

    render() {
        console.log(this.props);
        return(
            <Container>
                <Header>
                    <Body>
                    <Title>MyPosts</Title>
                    <Subtitle>{this.props.user.username}</Subtitle>
                    </Body>
                </Header>
                <Content>
                    <View style={{flexDirection: "row"}}>
                        <Button dark onPress={() => this.getPosts('title')}>
                            <Text>Title</Text>
                        </Button>
                        <Button dark onPress={() => this.getPosts('price')}>
                            <Text>Price</Text>
                        </Button>
                        <Button dark onPress={() => this.getPosts('date')}>
                            <Text>Date</Text>
                        </Button>
                        <Button dark onPress={() => this.props.changeScreen('home')}>
                            <Text>All Posts</Text>
                        </Button>
                        <Button dark onPress={() => this.props.changeScreen('add')}>
                            <Text>New</Text>
                        </Button>
                    </View>
                    {this.loadCards()}
                </Content>
                <Footer>
                    <Body>
                    <Button black onPress={() => this.props.changeScreen('login')}>
                        <Text>Log Out</Text>
                    </Button>
                    </Body>
                </Footer>
            </Container>
        )
    }


    loadCards()
    {
        let array = [];
        for (let i = 0; i < this.props.posts.length; i++) {
            array.push(<ExtraPostCard
                listIndex={i}
                key={i}
                title={this.props.posts[i].title}
                id={this.props.posts[i].id}
                description={this.props.posts[i].description}
                price={this.props.posts[i].price}
                date={this.props.posts[i].date}
            />);
        }
        return array;
    }

    getPosts(key)
    {
        this.props.getAllUser(this.props.user.id , key, this.state.order);
        if(this.state.order === 'ASC') {
            this.setState({ order: 'DESC'})
        } else {
            this.setState({order: 'ASC'})
        }
    }

}

const UserScreen = connect(
    (state) => {
        return {
            screen: state.screen,
            user: state.user,
            posts: state.posts
        }
    },
    {
        getAllUser, changeScreen
    }
)(AppUserScreen);

export default UserScreen;
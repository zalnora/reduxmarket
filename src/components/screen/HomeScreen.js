import React, {Component} from 'react';
import { connect } from 'react-redux';
import {Container, Header, Content, Body, Title, Subtitle, Text, Button, View, Footer} from 'native-base'
import PostCard from '../other/postCard'
import {getAll, changeScreen, setLoading} from '../../actions/index'



class AppHomeScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            order: 'ASC',
            hide: true
        }
    }

    componentDidMount(): void {
        this.getPosts('id');

    }

    render() {
        if(this.props.user.username !== 'quest')
        {
            return(
                <Container>
                    <Header>
                        <Body>
                        <Title>AllPosts</Title>
                        <Subtitle>{this.props.user.username}</Subtitle>
                        </Body>
                    </Header>
                    <Content>
                        <View style={{flexDirection: "row"}}>
                            <Button dark onPress={() => this.getPosts('title')}>
                                <Text>Title</Text>
                            </Button>
                            <Button dark onPress={() => this.getPosts('price')}>
                                <Text>Price</Text>
                            </Button>
                            <Button dark onPress={() => this.getPosts('date')}>
                                <Text>Date</Text>
                            </Button>
                            <Button dark onPress={() => this.props.changeScreen('user')}>
                                <Text>My Posts</Text>
                            </Button>
                        </View>
                        {this.loadCards()}
                    </Content>
                    <Footer>
                        <Body>
                        <Button black onPress={() => this.props.changeScreen('login')}>
                            <Text>Log Out</Text>
                        </Button>
                        </Body>
                    </Footer>
                </Container>
            )
        } else {
            return(
                <Container>
                    <Header>
                        <Body>
                        <Title>AllPosts</Title>
                        <Subtitle>{this.props.user.username}</Subtitle>
                        </Body>
                    </Header>
                    <Content>
                        <View style={{flexDirection: "row"}}>
                            <Button dark onPress={() => this.getPosts('title')}>
                                <Text>Title</Text>
                            </Button>
                            <Button dark onPress={() => this.getPosts('price')}>
                                <Text>Price</Text>
                            </Button>
                            <Button dark onPress={() => this.getPosts('date')}>
                                <Text>Date</Text>
                            </Button>
                        </View>
                        {this.loadCards()}
                    </Content>
                    <Footer>
                        <Body>
                        <Button black onPress={() => this.props.changeScreen('login')}>
                            <Text>Log Out</Text>
                        </Button>
                        </Body>
                    </Footer>
                </Container>
            )
        }

        }


        loadCards()
        {
            let array = [];
            for (let i = 0; i < this.props.posts.length; i++) {
                array.push(<PostCard
                    listIndex={i}
                    key={i}
                    title={this.props.posts[i].title}
                    description={this.props.posts[i].description}
                    price={this.props.posts[i].price}
                    date={this.props.posts[i].date}
                />);
            }
            return array;
        }

        getPosts(key)
        {
            this.props.getAll(key, this.state.order);
            if(this.state.order === 'ASC') {
                this.setState({ order: 'DESC'})
            } else {
                this.setState({order: 'ASC'})
            }
        }

    }

const HomeScreen = connect(
    (state) => {
        return {
            screen: state.screen,
            user: state.user,
            posts: state.posts
        }
    },
    {
        getAll, changeScreen, setLoading
    }
)(AppHomeScreen);

export default HomeScreen;
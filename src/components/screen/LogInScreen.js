import React, {Component} from 'react';
import { connect } from 'react-redux';
import {Item, Input, Button, Container, Header, Content} from 'native-base'
import {Text} from 'react-native'
import {logIn} from '../../actions/index'


class AppLogInScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
        };
    }
    render() {
        return(
            <Container>
                <Header />
                <Content>
                    <Item rounded>
                        <Input onChangeText={(text) => this.setState({username: text})} placeholder='Username'/>
                    </Item>
                    <Item rounded>
                        <Input onChangeText={(text) => this.setState({password: text})} placeholder='Password'/>
                    </Item>
                    <Button block success onPress={() => this.pressLogin()}>
                        <Text> Log In </Text>
                    </Button>
                    <Button block primary onPress={() => this.pressQuest()}>
                        <Text> Quest Mode</Text>
                    </Button>
                </Content>
            </Container>
        )
    }

    pressLogin() {
        const { username, password } = this.state;

        this.props.logIn({
            username,
            password,
        });
    }

    pressQuest() {
        const username = 'quest';
        const password = 'quest';

        this.props.logIn({
            username,
            password,
        });
    }
}

const LogInScreen = connect(
    (state) => {
        return {
            user: state.user
        }
    },
    {
        logIn
    }
)(AppLogInScreen);

export default LogInScreen;
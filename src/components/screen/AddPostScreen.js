import React, {Component} from 'react';
import { connect } from 'react-redux';
import {Item, Input, Button, Container, Header, Content, Body, Title, Subtitle} from 'native-base'
import {Text} from 'react-native'
import {addPost, changeScreen} from '../../actions/index'


class AppAddPostScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            userid: this.props.user.id,
            title: '',
            description: '',
            price: ''
        };
        console.log(this.props.user);
    }
    render() {
        return(
            <Container>
                <Header>
                    <Body>
                        <Title>New Post</Title>
                        <Subtitle>{this.props.user.username}</Subtitle>
                    </Body>
                </Header>
                <Content>
                    <Item rounded>
                        <Input onChangeText={(text) => this.setState({title: text})} placeholder='Title'/>
                    </Item>
                    <Item rounded>
                        <Input onChangeText={(text) => this.setState({description: text})} placeholder='Description'/>
                    </Item>
                    <Item rounded>
                        <Input onChangeText={(text) => this.setState({price: text})} placeholder='Price'/>
                    </Item>
                    <Button block success onPress={() => this.pressAdd()}>
                        <Text>Confirm</Text>
                    </Button>
                    <Button block primary onPress={() => this.pressCancel()}>
                        <Text>Cancel</Text>
                    </Button>
                </Content>
            </Container>
        )
    }

    pressAdd() {
        const { userid, title, description, price } = this.state;

        this.props.addPost({
            userid,
            title,
            description,
            price
        });
    }
    pressCancel() {
        this.props.changeScreen('user');
    }

}

const AddPostScreen = connect(
    (state) => {
        return {
            user: state.user
        }
    },
    {
        addPost, changeScreen,
    }
)(AppAddPostScreen);

export default AddPostScreen;
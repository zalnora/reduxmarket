import React, {Component} from 'react';
import { connect } from 'react-redux';
import {Spinner, Container, Header, Content} from 'native-base'
import {Text} from 'react-native'
import HomeScreen from './screen/HomeScreen'
import LogInScreen from './screen/LogInScreen'
import UserScreen from './screen/UserScreen'
import AddPostScreen from './screen/AddPostScreen'


class AppIndexComponent extends Component {
    render() {
        console.log(this.props.screen);
        if (this.props.loading){
            return(
                <Container>
                    <Header />
                    <Content>
                        <Spinner />
                        <Spinner color='blue' />
                    </Content>
                </Container>
            )
        } else if (this.props.screen === 'home') {
            return (
                <HomeScreen/>
            )
        } else if (this.props.screen === 'login') {
            return (
                <LogInScreen/>
            )
        } else if (this.props.screen === 'user') {
            return (
                <UserScreen/>
            )
        } else if (this.props.screen === 'add') {
            return (
                <AddPostScreen/>
            )
        }
        return (
            <Container>
                <Header />
                <Content>
                    <Text>Not Loading</Text>
                </Content>
            </Container>
        );
    }
}

const IndexComponent = connect(
    (state) => {
        return {
            loading: state.loading,
            screen: state.screen,
        }
        },
    null
)(AppIndexComponent);

export default IndexComponent;
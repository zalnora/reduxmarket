import {Text} from "react-native";
import React, {Component} from 'react';
import {View, Card, CardItem, Body} from 'native-base'

export default class AppPostCard extends Component {
    render() {
        return(
            <View>
                <Card>
                    <CardItem header>
                        <Text>{this.props.title}</Text>
                    </CardItem>
                    <CardItem bordered >
                        <Body>
                            <Text>{this.props.description}</Text>
                            <Text/>
                            <Text>Price: {this.props.price} €</Text>
                        </Body>
                    </CardItem>
                    <CardItem footer button>
                        <Text>{this.props.date}</Text>
                    </CardItem>
                </Card>
            </View>
        )
    }
}

import {Text} from "react-native";
import React, {Component} from 'react';
import {View, Card, CardItem, Body} from 'native-base'
import {connect} from "react-redux";
import {deletePost, changeScreen} from "../../actions";

class AppExtraPostCard extends Component {
    render() {
        return(
            <View>
                <Card>
                    <CardItem header>
                        <Text>id:</Text>
                        <Text>{this.props.id}</Text>
                        <Text> </Text>
                        <Text>{this.props.title}</Text>
                    </CardItem>
                    <CardItem bordered >
                        <Body>
                        <Text>{this.props.description}</Text>
                        <Text/>
                        <Text>Price: {this.props.price} €</Text>
                        </Body>
                    </CardItem>
                    <CardItem footer>
                        <Text>{this.props.date}</Text>
                    </CardItem>
                    <CardItem button dark onPress={() => this.props.deletePost(this.props.user.id, this.props.id)}>
                        <Text>Delete</Text>
                    </CardItem>
                </Card>
            </View>
        )
    }
}

const ExtraPostCard = connect(
    (state) => {
        return {
            user: state.user,
        }
    },
    {
        deletePost,
        changeScreen
    }
)(AppExtraPostCard);

export default ExtraPostCard;
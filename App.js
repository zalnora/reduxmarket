
import React, {Component} from 'react';
import { Provider } from 'react-redux';
import { store } from './src/store/index';
import IndexComponent from './src/components/index'

export default class App extends Component {
  render() {
    return (
        <Provider store={store}>
          <IndexComponent/>
        </Provider>
    );
  }
}

